let simplexNoise;

function setup() {
  createCanvas(400, 400);
  console.log(window);
  simplexNoise = new OpenSimplexNoise(Date.now());
}

function draw() {
  for (let x = 0; x < width; x++) {
    for (let y = 0; y < height; y++) {
      const value = (simplexNoise.noise2D(x, y) + 1) * 128;
      console.log(value);
    }
  }
}
