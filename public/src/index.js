import p5 from "p5/lib/p5.min";
import openSimplexNoise from "open-simplex-noise";

let increment = 0.02;
let zoff = 0;
let zincrement = 0.01;

const sketch = sk => {
  window.p5 = sk;
  let noise = new openSimplexNoise();

  sk.setup = () => {
    let maxRes = sk.windowWidth > sk.windowHeight ? "width" : "height";
    let res;

    if (maxRes === "width") {
      res = Math.ceil(sk.windowHeight / 3);
    } else {
      res = Math.ceil(sk.windowWidth / 3);
    }

    const canvas = sk.createCanvas(res, res);
    sk.pixelDensity(1);
    canvas.parent("canvas");
    sk.background(0);

    document.querySelector("#increment").value = increment;
    document.querySelector("#z-increment").value = zincrement;
    const cssCanvas = document.createElement("style");
    cssCanvas.type = "text/css";
    let styles;

    if (maxRes === "width") {
      styles = `
        #canvas canvas{
          width: 100vw !important;
          height: auto !important;
        }
      `;
    } else {
      styles = `
      #canvas canvas{
        height: 100vh !important;
        width: auto !important;
      }
    `;
    }

    cssCanvas.appendChild(document.createTextNode(styles));
    document.getElementsByTagName("head")[0].appendChild(cssCanvas);
  };

  sk.draw = () => {
    let xoff = 0;

    sk.loadPixels();
    for (let x = 0; x < sk.width; x++) {
      let yoff = 0;
      xoff += increment;
      for (let y = 0; y < sk.height; y++) {
        yoff += increment;

        let i = (x + y * sk.width) * 4;

        let bright = noise.noise3D(xoff, yoff, zoff) > 0 ? 255 : 0;
        //let bright = sk.random() > 0.5 ? 255 : 0;
        sk.pixels[i] = bright;
        sk.pixels[i + 1] = bright;
        sk.pixels[i + 2] = bright;
        sk.pixels[i + 3] = 255;
      }
    }

    sk.updatePixels();
    zoff += zincrement;
  };
};

document.querySelector("#increment").addEventListener("input", e => {
  increment = parseFloat(e.target.value);
});

document.querySelector("#z-increment").addEventListener("input", e => {
  console.log(e.target.value);
  zincrement = parseFloat(e.target.value);
});

// document.querySelector("#color").addEventListener("change", e => {
//   color = e.target.checked;
// });

// document.querySelector("#play").addEventListener("click", e => {
//   factorStep = 0.005;
//   factor = 0;
// });
// document.querySelector("#stop").addEventListener("click", e => {
//   factorStep = 0;
//   factor = 1;
//   document.querySelector("#factor").value = 1;
// });

document.querySelector("#config-button").addEventListener("click", e => {
  document.querySelector("#configs").style.display = "flex";
});
document.querySelector(".fa-times").addEventListener("click", e => {
  document.querySelector("#configs").style.display = "none";
});

const P5 = new p5(sketch);
